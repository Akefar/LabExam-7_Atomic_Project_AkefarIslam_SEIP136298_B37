<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!doctype html>
<html>
<head>
    <title>Profile Login Form Flat Responsive widget Template :: w3layouts</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Unique Login Form Widget Responsive, Login form web template,Flat Pricing tables,Flat Drop downs  Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- font files  -->
    <link href='../../../resource/asset2//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
    <link href='../../../resource/asset2//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <!-- /font files  -->
    <!-- css files -->
    <link href="../../../resource/asset2/css/animate-custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="../../../resource/asset2/css/style.css" rel='stylesheet' type='text/css' media="all" />
    <!-- /css files -->
</head>
<body>
<div class="header">
    <h1>Profile Picture </h1>
</div>
<div class="content">
    <section>
        <div id="container_demo" >
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>
            <div id="wrapper">
                <div id="login" class="animate form">
                    <div class="content-img">
                        <img src="../../../resource/asset2/images/profile.png" alt="img" class="w3l-img">
                    </div>
                    <form  action="store.php" autocomplete="on" method="post">
                        <h2>My Picture</h2>
                        <p>
                            <label for="username" class="uname" data-icon="u" > Your name</label>
                            <input id="username" name="name" required="required" type="text" placeholder="Enter Your Name"/>
                        </p>


                        <p class="login button">
                            <input type="file" name="image" >
                            <input type="submit" value="Submit" />
                        </p>


                </div>
            </div>
        </div>
    </section>
</div>
<div class="footer">
    <p>© 2016 Profile Login Form. All Rights Reserved | Design by <a href="https://w3layouts.com/" target="_blank">w3layouts</a></p>
</div>
</body>
</html>