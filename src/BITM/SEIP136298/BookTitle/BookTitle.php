<?php

namespace App\BookTitle;

use App\Database as DB;

use App\Message\Message;
use App\Utility\Utility;


use PDO;

class BookTitle extends DB{

    public $id="";

    public $book_title="";

    public $author_name="";



    public function __construct(){

        parent::__construct();

    }
    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('book_title',$data)){
            $this->book_title=$data['book_title'];
        }
        if(array_key_exists('author_name',$data)){
            $this->author_name=$data['author_name'];
        }
    }
    /*public function store(){

            $sql="insert into book_title(book_name,author_name)values('$this->book_title','$this->author_name')";

            $STH=$this->conn->prepare($sql);
            $STH->execute();
    }*/


    public function store(){

        $arrData  = array($this->book_title,$this->author_name);

        $sql = "INSERT INTO book_title ( book_name, author_name) VALUES ( ?, ?)";

        $STH = $this->conn->prepare($sql);

        $result=$STH->execute($arrData);

        if($result) {

            Message::message("<div id='msg'></div><h3 align='center'>[ Book Title: $this->book_title ] , [ Author Name: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3></div>");

        }
        else{
            Message::message("<div id='msg'></div><h3 align='center'>[ NAME: $this->book_title ] , [ BIRTH DATE: $this->author_name ] <br> Data Has not Been Inserted Successfully!</h3></div>");

        }



        Utility::redirect('create.php');

    }




    public function index(){



    }

}